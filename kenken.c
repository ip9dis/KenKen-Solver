/*
  Run with "-i <your-input-file.txt>" for interactive solving.
  Run with "-s <your-input-file.txt>" for automatic solving.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 5

typedef struct {
  int symbol;
  int goalNumber;
  char operator;
  int currentNumber;
  int ruleSatisfied;
} rule;

char kenken[N][N];
int kenkenValues[N][N];
rule * cages = NULL;

int getNumber(char* cageRule) {
  int number;
  char* pch;
  char funcString[60];
  
  strcpy(funcString, cageRule);
  pch = strtok (funcString, "+*");
  number = atoi(pch);

  return number;
}

char getOperator(char* cageRule) {
  char* pch;
  char funcString[60];
  char operator;

  strcpy(funcString, cageRule);
  pch = strtok(funcString, " ");
  operator = pch[strlen(pch)-1];
  
  return operator;
}

void createRuleArray(char* cageRule, int counter) {
  char* pch;
  char funcString[60];
  int x, y;

  strcpy(funcString, cageRule);
  pch = strtok(NULL, " ");
  while(pch != NULL) {
    sscanf(pch,"(%d,%d)", &x, &y);
    kenken[x-1][y-1] = (char)counter;
    pch = strtok(NULL, " ");
  }
}

int checkEntry(int x, int y, int val) {
  int valid = 0;
  int i, tmp;
  char opToCheck;
  int cageIndex;

  // Check horizontal line has no double.
  for(i=0;i<N;i++) {
    if(kenkenValues[x-1][i] == val) {
      valid = -1;
    }
  }

  // Check vertical line has no double.
  for(i=0;i<N;i++) {
    if(kenkenValues[i][y-1] == val) {
      valid = -2;
    }
  }

  // Get the right index for this cage.
  cageIndex = (int)kenken[x-1][y-1] - 97;
  // Get the right operator for this cage.
  opToCheck = cages[cageIndex].operator;

  // Modify the current value of the cage, according to the operator.
  if(opToCheck == '+') {
    tmp = cages[cageIndex].currentNumber + val;
  }
  else if(opToCheck == '*') {
    tmp = cages[cageIndex].currentNumber * val;
  }

  // If the current value is greater than the goal value exit with an error.
  if(tmp > cages[cageIndex].goalNumber) {
    valid = -3;
  }

  return valid;
}

int checkState(int numberOfRules) {
  int solved = 1;
  int i;
  int cagesCompleted = 0;

  // Check if any cage is NOT solved.
  for(i=0; i<numberOfRules; i++) {
    if(cages[i].ruleSatisfied != 1) {
      solved = 0;
      break;
    }
  }

  return solved;
}

void printState() {
  for(int i=0;i<N;i++) {
    for(int z=0;z<N;z++) {
      printf("+------");
    }
    printf("+\n");
    for(int j=0;j<N;j++) {
      printf("|  %d%c  ", kenkenValues[i][j], kenken[i][j]);
    }
    printf("|\n");
  }

  for(int z=0;z<N;z++) {
      printf("+------");
    }
  printf("+\n");
}

void printInstructions() {
  printf("Enter your command in the following format:\n"
   "+ i,j=val: for entering val at position(i,j)\n" 
   "+ i,j=0 : for clearing cell (i,j)\n"
   "+ 0,0=0 : for saving and ending the game\n"
   "Notice: i,j,val numbering is from [1..%d]\n", N);
}

void saveFile(char* inputFName) {
  FILE* pFile;
  char outputFName[20];
  sprintf(outputFName, "out-%s", inputFName);
  printf("%s", outputFName);
  pFile = fopen(outputFName, "w+");

  for(int i=0;i<N;i++) {
    for(int z=0;z<N;z++) {
      fprintf(pFile, "+------");
    }
    fprintf(pFile, "+\n");
    for(int j=0;j<N;j++) {
      fprintf(pFile, "|  %d%c  ", kenkenValues[i][j], kenken[i][j]);
    }
    fprintf(pFile, "|\n");
  }

  for(int k=0;k<N;k++) {
    fprintf(pFile, "+------");
  }
  fprintf(pFile, "+\n");

  fclose (pFile);
}

void modifyCage(int i,int j,int val) {
  int cageIndex = (int)kenken[i-1][j-1] - 97;
  char opToCheck = cages[cageIndex].operator;

  if(opToCheck == '+') {
    if(val == 0) {
      // If reseting cell value, subtract the cell's previous value.
      cages[cageIndex].currentNumber -= kenkenValues[i-1][j-1];      
    }
    else {
      cages[cageIndex].currentNumber += val;
    }
  }
  else if(opToCheck == '*') {
    if(val == 0) {
      // If reseting cell value, divide by cell's previous value.
      cages[cageIndex].currentNumber /= kenkenValues[i-1][j-1];
    }
    else {
      cages[cageIndex].currentNumber *= val;
    }
  }

  if(cages[cageIndex].currentNumber == cages[cageIndex].goalNumber) {
    cages[cageIndex].ruleSatisfied = 1;
  }
}

void play(char *inputFName, int numberOfRules) {
  int i, j, val;
  int correct_solution = 0;
  int checkValue = 0;
  int solved = 0;

  while(!correct_solution) {
    // Read input from the user.
    if (scanf("%d,%d=%d",&i,&j,&val) != 3) {
      while (getchar() != '\n') {};
      printf("Command is not in the right format.\n");
    }

    if (i > N || j > N) {
      // Inform user that he is out of bounds.
      printf("The indexs are out of bound!\n");
    }
    else if(val < 0 || val > N) {
      // Inform user that he cannot enter numbers below zero and beyond 9.
      printf("Valid cell numbers are 0-%d.", N);
    }
    else {
      if(i == 0 && j == 0 && val == 0) {
        // Terminate the game and save to output file.
        saveFile(inputFName);
        printf("The game has been saved to file 'out-%s'.\n", inputFName);
        break;
      }
      else if(val == 0 && (i > 0 && j > 0)) {
        // Modify the cage recordings.
        modifyCage(i, j, 0);

        // Reset the value of the cell to zero.
        kenkenValues[i-1][j-1] = 0;
      }
      else {
        // Handle new value insertion.
        if(kenkenValues[i-1][j-1] != 0) {
          // Inform user that he cannot override a cell value.
          printf("\nThe cell you have chosen already has a value.\n\n");
        }
        else {
          // Check if the state of the game is in correct solution.
          checkValue = checkEntry(i, j, val);

          if(checkValue == 0) {
            // Modify the cage recordings.
            modifyCage(i, j , val);

            // Change the cell value.
            kenkenValues[i-1][j-1] = val;

            // Check if the game is solved.
            solved = checkState(numberOfRules);

            if(solved) {
              printf("***Game solved***\n");
              printState();
              correct_solution = 1;
            }
            printState();
          }
          else {
            if(checkValue == -1) {
              printf("\nThere cannot be two cells with the same value in a horizontal line.\n");
            }
            else if(checkValue == -2) {
              printf("\nThere cannot be two cells with the same value in a vertical line.\n");
            }
            if(checkValue == -3) {
              printf("Invalid cell value. Check the cage rules and try again.\n");
            }
            printState();
            //printInstructions();
          }
        }
      }
    }  
  }
}

void readInputFile(char* fileName, char displayCageRules[], int* numberOfRules) {
  char ruleLine[120];
  FILE* fp;
  int goalNumber = 0;
  char buffer[50];
  char ruleOperator;
  int puzzleSize = 0;
  int counter = 97;
  int cageCounter = 1;

  // Open the file.
  fp = fopen (fileName, "r");

  // Get the size of the puzzle.
  fgets(buffer, 10, fp);
  puzzleSize = atoi(buffer);

  while(fgets (ruleLine, 60, fp) != NULL) {
    // Read each line and process the cage rules.
    
    goalNumber = getNumber(ruleLine); // Get the cage goal number.
    ruleOperator = getOperator(ruleLine); // Get the cage operator.

    // Alocate space for the new structure, representing the cages and its rules.
    cages = (rule*)realloc(cages, sizeof(rule) * cageCounter);

    // Initialize structure.
    cages[cageCounter-1].symbol = counter;
    cages[cageCounter-1].operator = ruleOperator;
    cages[cageCounter-1].goalNumber = goalNumber;
    cages[cageCounter-1].ruleSatisfied = 0;
    cages[cageCounter-1].currentNumber = (ruleOperator == '+') ? 0 : 1;

    // Set the cell values of the array according to the symbol.
    createRuleArray(ruleLine, counter);
    
    // Create a rule string and concatinate with the other rules.
    sprintf(buffer, "%c=%d%c ", (char)counter, goalNumber, ruleOperator);
    strcat(displayCageRules, buffer);

    // Step forward.
    counter++;
    cageCounter++;
    (*numberOfRules)++;
  }

  // Close the file.
  fclose(fp);
}

int kenkenSolver(int row, int column, int numberOfRules) {
  int cellValue;

  if(checkState(numberOfRules)) {
    return 1;
  }

  for(cellValue=1; cellValue<=N; cellValue++) {
    if(checkEntry(row+1, column+1, cellValue) == 0) {
      kenkenValues[row][column] = cellValue;
      modifyCage(row+1, column+1, cellValue);
      if(column == (N-1)) {
        if (kenkenSolver(row+1, 0, numberOfRules)) return 1;
      }else {
        if (kenkenSolver(row, column+1, numberOfRules)) return 1;
      }
      modifyCage(row+1, column+1, 0);
      kenkenValues[row][column] = 0;
    }
  }

  return 0;
}

int main(int argc, char **argv) {
  char displayCageRules[100];
  int numberOfRules = 0;
  int* pNumberOfRules = &numberOfRules;

  // Read the input file and create the rules.
  readInputFile(argv[2], displayCageRules, pNumberOfRules);

  //printf("%s\n", displayCageRules);
  if(strcmp(argv[1], "-i") == 0) {
    printState();
    printInstructions();
    play(argv[2], numberOfRules);
  }
  else if(strcmp(argv[1], "-s") == 0) {
    if(kenkenSolver(0, 0, numberOfRules)) {
      printf("Solved!\n");
      printState();
    }
    else {
      printf("This KenKen is is probably something wrong with the cage rules.\n");
    }
    
  }

  return 0;
}